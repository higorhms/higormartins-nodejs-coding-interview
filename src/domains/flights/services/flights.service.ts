import { Flight, FlightsModel } from '../models/flights.model'

export class FlightsService {
    async getAll() {
        return FlightsModel.find()
    }

    async findById(id: string) {
        try {
        return FlightsModel.findById(id)
        } catch (error) {
        return undefined
        }
    }

    async update(id: string, flight: Flight) {
        await FlightsModel.updateOne({ _id: id }, { $set: flight })
    }
}
