import { JsonController, Get, Post, Body, Param } from 'routing-controllers'
import { FlightsService } from '../domains/flights/services/flights.service'
import { Person } from '../domains/persons/models/persons.model'

const flightsService = new FlightsService()

@JsonController('/flights', { transformResponse: false })
export default class FlightsController {
    @Get('')
    async getAll() {
        return {
            status: 200,
            data: await flightsService.getAll(),
        }
    }

    @Post('/:id/persons')
    async addPersonToFlight(
        @Param("id") id: string,
        @Body() person: Person
        ) {

        const flight = await flightsService.findById(id)            

        if(!flight){
            return {
                status: 404,
                message: 'Flight not found'
            }
        }

        flight.passengers.push(person)
        
        console.log(flight)

        await flightsService.update(flight._id, flight)

        return {
            status: 201,
        }
    }
}
